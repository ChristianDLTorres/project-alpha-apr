from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTasktForm
from tasks.models import Task


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/tasklist.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTasktForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")

    else:
        form = CreateTasktForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/createtask.html", context)

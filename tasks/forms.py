from django import forms
from django.contrib.auth.models import User
from tasks.models import Task


class CreateTasktForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]

        owner = forms.ModelChoiceField(
            queryset=User.objects.all(), empty_label=None
        )

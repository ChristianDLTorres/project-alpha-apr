from django import forms
from django.contrib.auth.models import User
from projects.models import Project


class CreateProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]

        owner = forms.ModelChoiceField(
            queryset=User.objects.all(), empty_label=None
        )
